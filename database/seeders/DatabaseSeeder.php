<?php

namespace Database\Seeders;
use App\Models\User;
use App\Models\Ad;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $boards = [];
        $users = \App\Models\User::factory(10)->create();

        $users->each(function(User $user) {
            $ads = \App\Models\Ad::factory(1)->create(['author_id' => $user->id]);
        });
    }
}
