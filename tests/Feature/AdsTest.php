<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Ad;

class AdsTest extends TestCase
{
    use RefreshDatabase;

    public function test_empty_form()
    {
        $response = $this->post('/login', []);

        $response->assertSessionHasErrors(['email', 'password']);
    }

    public function test_mainpage()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_new_user_sign_up()
    {
        $user = User::factory()->make();

        $attributes = [
            'email' => $user->email,
            'password' => '123456789',
        ];

        $response = $this->post('/login', $attributes);

        $response->assertRedirect('/');

        $this->assertDatabaseHas('users', ['email' => $user->email]);

        $this->assertAuthenticated();
    }

    public function test_existing_user_sign_in()
    {
        $user = User::factory()->create();

        $attributes = [
            'email' => $user->email,
            'password' => 'password',
        ];

        $response = $this->post('/login', $attributes);

        $response->assertRedirect('/');

        $this->assertAuthenticated();
    }

    public function test_existing_user_sign_in_failed()
    {
        $user = User::factory()->create();

        $attributes = [
            'email' => $user->email,
            'password' => 'wrong_password',
        ];

        $response = $this->post('/login', $attributes);

        $response->assertRedirect('/');

        $this->assertGuest();
    }

    public function test_can_user_logout()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)
                ->get('logout');

        $response->assertRedirect('/');

        $this->assertGuest();
    }

    public function test_unauthorized_redirect()
    {
        $response = $this->get('/ads/create');

        $response->assertRedirect('/login');
    }

    public function test_mainpage_five_ads()
    {
        $ads = Ad::factory(10)->create();
        $ads = Ad::orderBy('created_at', 'desc')->take(5)->get();

        $plucked = $ads->pluck('title');
        $response = $this->get('/');
        $response->assertSeeTextInOrder($plucked->toArray());
    }

    public function test_user_edit_not_his_ad()
    {
        $user = User::factory()->create();
        $ad = Ad::factory()->create(['author_id' => User::factory()->create()]);
        $response = $this->actingAs($user)
                ->get('/ads/'.$ad->id.'/edit');
        $response->assertForbidden();
    }

    public function test_user_edit_his_ad()
    {
        $user = User::factory()->create();
        $ad = Ad::factory()->create(['author_id' => $user->id]);
        $response = $this->actingAs($user)
                ->get('/ads/'.$ad->id.'/edit');
         $response->assertStatus(200);
    }

    public function test_user_delete_not_his_ad()
    {
        $user = User::factory()->create();
        $ad = Ad::factory()->create(['author_id' => User::factory()->create()]);
        $response = $this->actingAs($user)
                ->delete('/ads/'.$ad->id);
        $response->assertForbidden();
    }

    public function test_user_delete_his_ad()
    {
        $user = User::factory()->create();
        $ad = Ad::factory()->create(['author_id' => $user->id]);
        $response = $this->actingAs($user)
                ->delete('/ads/'.$ad->id);
        $this->assertDatabaseMissing('ads', $ad->toArray());
        $response->assertRedirect('/ads');
    }
}
