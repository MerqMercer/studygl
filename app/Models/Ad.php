<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    
    protected $fillable = [
        'title',
        'description',
        'author_id',
    ];
    
    use HasFactory;
    
    public function author() {
        return $this->belongsTo(User::class, 'author_id', 'id');
    }
}
