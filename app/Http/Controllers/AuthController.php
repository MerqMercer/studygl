<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\Ad;
use App\Models\User;

class AuthController extends Controller
{
    public function login(Request $request) {

       $credentials = $request->validate([
           'email' => ['required'],
           'password' => ['required']
       ]);

       $user = User::query()->where('email', '=' ,$credentials['email'])->first();

       if(null === $user) {

           $credentials = $request->validate([
                'email' => ['required', 'unique:users,email'],
                'password' => ['required']
           ]);

           $user = User::create([
               'first_name' => '',
               'last_name' => '',
               'email' => $credentials['email'],
               'password' => Hash::make($credentials['password'])
           ]);
       } else {
           if(!Hash::check($credentials['password'], $user->password)) {
                    return back()->withErrors(['email' => 'The provided credentials do not match our records.']);
           }
       }

       Auth::login($user);

       $request->session()->regenerate();

       return back();
    }

    public function logout(Request $request) {

       Auth::logout();

       $request->session()->invalidate();
       $request->session()->regenerateToken();

       return back();
    }
}