<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class GitlabController extends Controller
{
    public function callback(Request $request) {

       $response = Http::withHeaders(['Accept' => 'application/json'])
            ->asForm()
            ->post('https://gitlab.com/oauth/token', [
                'client_id' => config('oauth.gitlab.client_id'),
                'client_secret' => config('oauth.gitlab.client_secret'),
                'code' => $request->get('code'),
                'grant_type' => 'authorization_code',
                'redirect_uri' => config('oauth.gitlab.callback_uri'),
        ]);
       $token = $response['access_token'];

       $response = Http::withHeaders(['Authorization' => 'Bearer ' . $token])
               ->get('https://gitlab.com/api/v4/user');

       $user = User::query()
               ->where('email', '=', $response['public_email'])
               ->first();

       if($user === null) {
           $user = User::create([
               'first_name' => $response['name'],
               'last_name' => '',
               'email' => $response['public_email'],
               'password' => Hash::make(Str::random(8))
           ]);
       }

       Auth::login($user);

       $request->session()->regenerate();

       return back();
    }

}
