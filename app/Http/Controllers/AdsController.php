<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Ad;

class AdsController extends Controller
{
    public function index() {
        $ads = Ad::with(['author'])->orderBy('created_at', 'desc')->paginate(5);

        return view('ads.index', compact('ads'));
    }

    public function create() {
        return view('ads.create');
    }

    public function store(Request $request) {

        $request->validate([
            'title' => ['required', 'min:3', 'max:255'],
            'description' => ['required'],
        ]);

        $data = $request->all();
        $data['author_id'] = Auth::id();
        Ad::create($data);

        return redirect()->route('ads.index');
    }

    public function show($id) {
        $ad = Ad::find($id);
        return view('ads.show', compact('ad'));
    }

    public function edit(Ad $ad) {
        return view('ads.update', compact('ad'));
    }

    public function update(Request $request, Ad $ad) {
        $request->validate([
            'title' => ['required', 'min:3', 'max:255'],
            'description' => ['required'],
        ]);

        $ad->update($request->all());
        return redirect()->route('ads.index');
    }

    public function destroy(Ad $ad) {
       $ad->delete();
       return redirect()->route('ads.index');
    }
}
