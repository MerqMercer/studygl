<?php

namespace App\Services;

class GitlabService
{
    public static function link()
    {
        $parameters = [
            'client_id' => config('oauth.gitlab.client_id'),
            'redirect_uri' => config('oauth.gitlab.callback_uri'),
            'scope' => 'api read_user read_api openid profile email',
            'response_type' => 'code',
        ];

        return 'https://gitlab.com/oauth/authorize?' . http_build_query($parameters);
    }

}

