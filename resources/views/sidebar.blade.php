<div class="sidebar">
    @guest
    <form class="" method="POST" action="{{ route('login') }}">
        <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input name="email" type="text" class="form-control @error('email') is-invalid @enderror" id="email" value='' placeholder="Email">
            @error('email')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input name="password" type="password" class="form-control @error('password') is-invalid @enderror" id="password" value='' placeholder="Password">
            @error('password')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>

        <div class="mb-3">
            <button class="btn btn-primary" type="submit">Login</button>
        </div>
        @csrf
    </form>   
    <br>
    <a href="{{ \App\Services\GitlabService::link() }}" class="btn btn-primary">Login via Gitlab</a>
    @endguest
    
    @auth
    <div>
        <form class="" method="GET" action="{{ route('logout') }}">
            <div class="mb-3">
                <button class="btn btn-primary" type="submit">Logout</button>
            </div>
            @csrf
        </form>
    </div>   
    <div>
        <a href="{{route('ads.create')}}" class="btn btn-primary">Create ad</a>
    </div>
    @endauth
</div>
