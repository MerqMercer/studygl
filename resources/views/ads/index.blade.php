@extends('layout')

@section('ads')
@foreach($ads as $ad)
<a href="{{ route('ads.show', ['id' => $ad->id])}}"><h2> {{$ad->title}} </h2></a>
<p> {{$ad->description}} </p>
<br>
<span> {{$ad->author->first_name}} {{$ad->author->last_name}}</span>
<br>
<i>{{$ad->created_at->diffForHumans()}}</i>
@can('update', $ad)
<a href="{{ route('ads.edit', ['ad'=> $ad->id]) }}" type="button" class="btn btn-light">Edit</a>
@endcan

@can('delete', $ad)
<form action='{{ route('ads.destroy', ['ad' => $ad->id])}}' method='post'>
    @method('delete')
    @csrf
    <input type='submit' class='btn btn-light' value='Delete' />
</form>
@endcan
<hr>
@endforeach
{{ $ads->links() }}
@endsection