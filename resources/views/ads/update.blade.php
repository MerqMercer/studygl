@extends('layout')

@section('ads')
<form class="" method="POST" action="{{route('ads.update', ['ad' => $ad->id])}}">
    @method('put')
    <div class="mb-3">
        <label for="title" class="form-label">Title</label>
        <input name="title" type="text" class="form-control @error('title') is-invalid @enderror" id="title" value='{{old('title', $ad->title)}}' placeholder="title">
        @error('title')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="mb-3">
        <label for="description" class="form-label">Description</label>
        <textarea name="description" class="form-control @error('description') is-invalid @enderror" id="description" >{{old('description', $ad->description)}}</textarea>
        @error('description')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>


    <div class="mb-3">
        <button class="btn btn-primary" type="submit">Update ad</button>
    </div>
    @csrf
</form>
@endsection