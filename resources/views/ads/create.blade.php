@extends('layout')

@section('ads')
<!--<form class="was-validated">-->
<form class="" method="POST" action="{{route('ads.store')}}">
    <div class="mb-3">
        <label for="title" class="form-label">Title</label>
        <input name="title" type="text" class="form-control @error('title') is-invalid @enderror" id="title" value='{{old('title')}}' placeholder="title">
        @error('title')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="mb-3">
        <label for="description" class="form-label">Description</label>
        <textarea name="description" class="form-control @error('description') is-invalid @enderror" id="description" >{{old('description')}}</textarea>
        @error('description')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>


    <div class="mb-3">
        <button class="btn btn-primary" type="submit">Create ad</button>
    </div>
    @csrf
</form>
@endsection