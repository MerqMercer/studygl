<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', [\App\Http\Controllers\AdsController::class, 'index']);

Route::middleware('auth')->name('ads.')->prefix('ads')->group(function() {
    Route::get('/', [\App\Http\Controllers\AdsController::class, 'index'])->name('index');
    Route::get('/create', [\App\Http\Controllers\AdsController::class, 'create'])->name('create');
    Route::post('/', [\App\Http\Controllers\AdsController::class, 'store'])->name('store');
    Route::get('/{ad}/edit', [\App\Http\Controllers\AdsController::class, 'edit'])->can('update', 'ad')->name('edit');
    Route::put('/{ad}/', [\App\Http\Controllers\AdsController::class, 'update'])->can('update', 'ad')->name('update');
    Route::delete('/{ad}/', [\App\Http\Controllers\AdsController::class, 'destroy'])->can('delete', 'ad')->name('destroy');
});

Route::name('ads.')->prefix('ads')->group(function() {
    Route::get('/{id}', [\App\Http\Controllers\AdsController::class, 'show'])->name('show');
});

Route::post('login', [\App\Http\Controllers\AuthController::class, 'login'])->middleware('guest');
Route::get('logout', [\App\Http\Controllers\AuthController::class, 'logout'])->middleware('auth')->name('logout');

Route::get('login', fn() => redirect('/'))->name('login');
Route::get('/callback', [\App\Http\Controllers\GitlabController::class, 'callback'])->middleware('guest');